###################################
#### Author: AKSHAY GIRULKAR ######
###################################

import util.utils as utils


# Wrapper for every extraction from the resumes
def get_info_from_resumes(resume_data):
    person_name = ''
    for name in utils.extract_name(resume_data):
        person_name += '' + name

    print("Person name ==> ", person_name)
    print("Person Email ==> ", utils.extract_email(resume_data))
    print("Person Number ==> ", utils.extract_mobile_number(resume_data))
    print("Person Education ==> ", utils.extract_education(resume_data))
    print("Person Skills ==> ", utils.extract_skills(resume_data))
    print("Person Industrial Experience ==>", utils.extract_experience(resume_data))
