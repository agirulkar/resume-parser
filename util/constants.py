###################################
#### Author: AKSHAY GIRULKAR ######
###################################
import pandas as pd

# Education details List Should be in Capital case only
EDUCATION = pd.read_csv("../csv/education.csv").columns.tolist()

# Skills details List Should be in Capital case only
SKILLS = pd.read_csv("../csv/skills.csv").columns.tolist()
