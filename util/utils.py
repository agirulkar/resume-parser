###################################
#### Author: AKSHAY GIRULKAR ######
###################################
import spacy
import nltk
import re
from spacy.matcher import Matcher
from nltk.corpus import stopwords
from util.constants import EDUCATION
from util.constants import SKILLS
from nltk.stem import WordNetLemmatizer

nlp = spacy.load('en_core_web_sm')

# Get all general stop words
STOPWORDS = set(stopwords.words('english'))

# initialize matcher with a english vocabulary
matcher = Matcher(nlp.vocab)


# Extract Name from Resume
def extract_name(resume_data):
    nlp_text = nlp(resume_data)

    # First name, middle Name and Last name are always Proper Nouns
    pattern = [{'POS': 'PROPN'},
               {'POS': 'PROPN'},
               {'POS': 'PROPN', 'OP': '?'}]

    matcher.add('NAME', None, pattern)

    matches = matcher(nlp_text)

    for match_id, start, end in matches:
        span = nlp_text[start:end]
        return span.text


# Extract Mobile Number from Resume
def extract_mobile_number(resume_data):
    phone = re.findall(re.compile(
        r'(?:(?:\+?([1-9]|[0-9][0-9]|[0-9][0-9][0-9])\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([0-9][1-9]|[0-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?'),
        resume_data)

    if phone:
        number = ''.join(phone[0])
        if len(number) > 10:
            return '+' + number
        else:
            return number


# Extract Email from Resume
def extract_email(resume_data):
    email = re.findall("([^@|\s]+@[^@]+\.[^@|\s]+)", resume_data)
    if email:
        try:
            return email[0].split()[0].strip(';')
        except IndexError:
            return None


# Extract Education details from Resume
def extract_education(resume_data):
    nlp_text = nlp(resume_data)
    noun_chunks = nlp_text.noun_chunks

    # Sentence Tokenizer
    nlp_text = [sent.string.strip() for sent in nlp_text.sents]

    edu = {}
    # Extract education degree
    for index, text in enumerate(nlp_text):
        for tex in text.split():
            # Replace all special symbols
            tex = re.sub(r'[?|$|.|!|,|(|)]', r'', tex)  # added ( )
            # print("text ===>", tex)
            if tex.upper() in EDUCATION and tex not in STOPWORDS:
                edu[tex] = text + nlp_text[index]

    education = []
    for key in edu.keys():
        education.append(key)
    # for key in edu.keys():
    #     year = re.search(re.compile(r'(((20|19)(\d{2})))'), edu[key])
    #     if year:
    #         education.append((key, ''.join(year[0])))
    #     else:
    #         education.append(key)
    return education


# Extract Skills from Resume
def extract_skills(resume_data):
    nlp_text = nlp(resume_data)
    noun_chunks = nlp_text.noun_chunks

    # removing stop words and implementing word tokenization
    tokens = [token.text for token in nlp_text if not token.is_stop]

    skillset = []

    # check for one-grams (example: python)
    for token in tokens:
        if token.upper() in SKILLS:
            skillset.append(token)

    # check for bi-grams and tri-grams (example: machine learning)
    for token in noun_chunks:
        token = token.text.upper().strip()
        if token in SKILLS:
            skillset.append(token)

    return [i.upper() for i in set([i.upper() for i in skillset])]


def extract_experience(resume_data):
    lines = [el.strip() for el in resume_data.split("\n") if len(el) > 0]  # Splitting on the basis of newlines
    lines = [nltk.word_tokenize(el) for el in lines]  # Tokenize the individual lines
    lines = [nltk.pos_tag(el) for el in lines]  # Tag them
    experience_list = []
    for sent in lines:
        sen = " ".join([words[0].lower() for words in sent])
        if re.search('experience', sen):
            sen_tokenised = nltk.word_tokenize(sen)
            tagged = nltk.pos_tag(sen_tokenised)
            entities = nltk.chunk.ne_chunk(tagged)
            for subtree in entities.subtrees():
                for leaf in subtree.leaves():
                    if leaf[1] == 'CD':
                        experience = leaf[0]
                        experience_list = experience

    return experience_list
