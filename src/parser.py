###################################
#### Author: AKSHAY GIRULKAR ######
###################################

import os
import wrapper.resume_wrapper as wrapper
import util.resume_reader as resume_reader

resume_data = ''
# Directory where Resumes are kept
input_dir = '../input_resumes'
try:
    files_to_process = os.listdir(input_dir)
    # Process every Resume in that directory
    for file in files_to_process:
        # Check whether file is docx or PDF
        if file.split('.')[-1] == 'docx':
            resume_data = resume_reader.extract_text_from_docx(input_dir + '/' + file)
            # print(resume_data)
            wrapper.get_info_from_resumes(resume_data)
            print("===========================================")
            # pass
        elif file.split('.')[-1] == 'pdf':
            for page in resume_reader.extract_text_from_pdf(input_dir + '/' + file):
                resume_data += ' ' + page
            wrapper.get_info_from_resumes(resume_data)
            print("===========================================")
except Exception as e:
    print("Resume Parser Exception : " + str(e))
